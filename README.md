Source code for the [Temporalitäten von (Transport-)Infrastrukturen Workshop website](https://kritis.gitlab.io/tticonf2021/) taking place on the 11th of May, 2021, hosted by the [Research Training Group KRITIS](https://www.kritis.tu-darmstadt.de/).

Web design informed by current best practices for [Resilient Web Design](https://resilientwebdesign.com/).

Web page styling based on the pretty rad [Bulma CSS framework](https://www.bulma.io/).

Serverless web form functionality provided by [BASIN](https://www.usebasin.com/).
